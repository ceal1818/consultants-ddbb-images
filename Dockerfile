FROM mysql:5.7

ENV MYSQL_ROOT_PASSWORD 1234
ENV MYSQL_USER node
ENV MYSQL_PASSWORD 1234
ENV MYSQL_DATABASE consultants

EXPOSE 3306

COPY backup/consultants-backup.sql /docker-entrypoint-initdb.d

